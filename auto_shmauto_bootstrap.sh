#!/bin/sh
# $Id: auto_shmauto_bootstrap.sh 1092 2006-11-16 03:02:42Z flaterco $

# Inputs:
#   Makefile.am
#   configure.ac
# Outputs:
#   Makefile.in
#   aclocal.m4
#   autom4te.cache
#   config.guess
#   config.sub
#   configure
#   depcomp
#   install-sh
#   ltmain.sh
#   missing

autoreconf --install
