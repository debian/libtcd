#!/bin/sh
# $Id: auto_shmauto_clean.sh 1092 2006-11-16 03:02:42Z flaterco $

# Get rid of everything that could be regenerated.

make distclean
rm -f           \
 Makefile.in    \
 aclocal.m4     \
 config.guess   \
 config.sub     \
 configure      \
 depcomp        \
 install-sh     \
 ltmain.sh      \
 missing
rm -rf autom4te.cache
